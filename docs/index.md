---
title: Notas - Redes - 2023-2
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Notas - Redes - 2023-2

--------------------------------------------------------------------------------

## Recursos

<!--
Notas - Redes - 2023-2

- <https://Redes-Ciencias-UNAM.gitlab.io/2023-2/notas>
-->

Mecanismos de contacto para dudas y preguntas:

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Correo electrónico | <a class="email" href="mailto&colon;&#114;&#x65;&#100;&#x65;&#x73;&commat;&#x63;&#x69;&#x65;&#110;&#x63;&#x69;&#x61;&#x73;&#46;&#x75;&#110;&#x61;&#x6d;&#46;&#x6d;&#120;">&#114;&#x65;&#100;&#x65;&#x73;@&#x63;&#x69;&#x65;&#110;&#x63;&#x69;&#x61;&#x73;&#46;&#x75;&#110;&#x61;&#x6d;&#46;&#x6d;&#120;</a>
| Grupo de chat      | <a href="https://&#116;&#46;&#x6d;&#x65;/&#114;&#x65;&#100;&#x65;&#x73;&#x5f;&#x63;&#x69;&#x65;&#110;&#x63;&#x69;&#x61;&#x73;&#x5f;&#x75;&#110;&#x61;&#x6d;">https://&#116;&#46;&#x6d;&#x65;/&#114;&#x65;&#100;&#x65;&#x73;&#x5f;&#x63;&#x69;&#x65;&#110;&#x63;&#x69;&#x61;&#x73;&#x5f;&#x75;&#110;&#x61;&#x6d;</a>

Ligas de interés

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Página del curso | <https://redes-ciencias-unam.gitlab.io/>
| Lista de reproducción en YouTube para el semestre 2023-2 | <https://tinyurl.com/Redes-Ciencias-UNAM-YT-2023-2>
| Formulario de características de hardware y software | <https://forms.gle/Akn6YWDxTVEZ4qWo9>
| Flujo de trabajo para la entrega de actividades | <https://redes-ciencias-unam.gitlab.io/workflow/>
| Repositorio para la entrega de actividades | <https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes>
| Merge requests de las entregas de actividades | <https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests>
| Página con las actividades entregadas | <https://Redes-Ciencias-UNAM.gitlab.io/2023-2/tareas-redes>

--------------------------------------------------------------------------------

Actividades del semestre

| Actividades | URL
|:-----------:|:---------------------------------------|
| Tareas      | <https://redes-ciencias-unam.gitlab.io/tareas/>
| Laboratorio | <https://redes-ciencias-unam.gitlab.io/laboratorio/>
| Proyectos   | <https://redes-ciencias-unam.gitlab.io/proyectos/>
| Examenes    | <https://redes-ciencias-unam.gitlab.io/examenes/>

--------------------------------------------------------------------------------

## Semana 1	📓

<a name="presentacion" id="presentacion"></a>

- Presentación

### Lunes - 2023-01-30

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Presentación del curso | <https://redes-ciencias-unam.gitlab.io/curso/>
| Temario del curso | <https://redes-ciencias-unam.gitlab.io/curso/temario/>

### Martes - 2023-01-31

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="tarea-1" id="tarea-1"></a>

!!! note
    **Tarea 1**: Flujo de trabajo en GitLab

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-1/>

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Control de versiones con GIT | <https://tonejito.github.io/curso-git/>
| Control de versiones con GIT | <https://www.youtube.com/watch?v=sLSqGjY84Sg&list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&index=1>
| Flujo de trabajo para la entrega de actividades | <https://redes-ciencias-unam.gitlab.io/workflow/>
| Instalar dependencias de desarrollo para el flujo de trabajo | <https://redes-ciencias-unam.gitlab.io/workflow/instalar-dependencias/>

### Miércoles - 2023-02-01

<a name="tarea-2" id="tarea-2"></a>

!!! note
    **Tarea 2**: Curso de nivelación de GNU/Linux

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-2/>

    Crear una nueva rama `tarea-2` a partir de la rama `entregas`:

    ```
    $ git checkout entregas
    $ git checkout -b tarea-2
    ```

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Crear cuenta en el sitio de Red Hat | <https://www.redhat.com/>
| Avanzar con el curso de nivelación de GNU/Linux | <https://www.redhat.com/en/services/training/rh024-red-hat-linux-technical-overview>

### Jueves - 2023-02-02

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Finalizar el ejercicio guiado del flujo de trabajo | <https://redes-ciencias-unam.gitlab.io/workflow/>
| Crear el _fork_ y el _merge request_ que son considerados como la `tarea-1` | <https://redes-ciencias-unam.gitlab.io/tareas/tarea-1/>

### Viernes - 2023-02-03

&nbsp;

--------------------------------------------------------------------------------

## Semana 2	📓

<a name="introduccion" id="introduccion"></a>

- Introducción

### Lunes - 2023-02-06

<div class="dia-feriado">Día feriado</div>

### Martes - 2023-02-07

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="tarea-3" id="tarea-3"></a>

!!! note
    **Tarea 3**: Instalación de Cisco Packet Tracer

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-3/>

    Crear una nueva rama `tarea-3` a partir de la rama de la actividad anterior (`tarea-2`):

    ```
    $ git checkout tarea-2
    $ git checkout -b tarea-3
    ```

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Crear cuenta en el sitio Skills for All | <https://skillsforall.com/>
| Avanzar con el curso de Packet Tracer | <https://skillsforall.com/course/getting-started-cisco-packet-tracer>

### Miércoles - 2023-02-08

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Introducción (parte 1) | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=SMt3Fs-n4bM&index=2>
| Introducción (parte 2) | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=fF2q8vhmjfs&index=3>
| Introducción (parte 3) | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=GCg_vA_DuL0&index=4>

### Jueves - 2023-02-09

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

### Viernes - 2023-02-10

<a name="entrega-tarea-1" id="entrega-tarea-1"></a>

!!! warning
    Entrega `tarea-1`

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-1>

--------------------------------------------------------------------------------

## Semana 3	📔

<a name="capa-1" id="capa-1"></a>

- **Capa 1** - Física

### Lunes - 2023-02-13

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="entrega-tarea-2" id="entrega-tarea-2"></a>

!!! warning
    Entrega `tarea-2`

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-2>

<a name="practica-1" id="practica-1"></a>

!!! note
    **Práctica 1**: Laboratorio virtual de redes `¹`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-1>

    `¹`: Esta práctica debe ser entregada **individualmente**, <u>todas las demás son **en equipo**</u>

### Martes - 2023-02-14

<a name="entrega-tarea-3" id="entrega-tarea-3"></a>

!!! warning
    Entrega `tarea-3`

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-3>

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Comparativa entre modelo OSI y modelo TCP/IP | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=gAUFrpBoOUw&index=5>
| Tipos de transmisión: unicast, multicast, broadcast y anycast | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=wCZ3_TFoF_0&index=6>
| Mecanismos de transmisión: simplex, half duplex y full duplex | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=x9XDX5KegP4&index=7>

### Miércoles - 2023-02-15

<!-- -->
<a name="examen-prueba" id="examen-prueba"></a>

!!! danger
    Examen de prueba en Moodle

    - <https://moodle.fciencias.unam.mx/cursos/mod/quiz/view.php?id=68559>
<!--
    Revisar la liga en Telegram

    - <https://t.me/redes_ciencias_unam/9314>

-->

### Jueves - 2023-02-16

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Capa física: Cableado estructurado | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=YiMS8Wo42uc&index=8>
| Capa física y medios de transmisión | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=IYAro0jRPdg&index=9>
| Capa física: Topologías de red | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=pV70x_ZRvBw&index=10>
| Dispositivos activos de una red | <https://www.youtube.com/watch?list=PLN1TFzSBXi3TYgnUzlVsjdWYLBAaNdq0v&v=nbmmGsRhNCo&index=11>

### Viernes - 2023-02-17

&nbsp;

--------------------------------------------------------------------------------

## Semana 4	📔

<a name="capa-2" id="capa-2"></a>

- **Capa 2** - Enlace

### Lunes - 2023-02-20

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-2" id="practica-2"></a>

!!! note
    **Práctica 2**: Configuración de switches administrables

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-2>

### Martes - 2023-02-21

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="entrega-practica-1" id="entrega-practica-1"></a>

!!! warning
    Entrega `practica-1` `¹`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-1>

    `¹`: Esta práctica debe ser entregada **individualmente**, <u>todas las demás son **en equipo**</u>

<!--
    Revisar esta serie de mensajes para aclaración sobre las ramas

    - <https://t.me/redes_ciencias_unam/9306>
-->

### Miércoles - 2023-02-22

&nbsp;

### Jueves - 2023-02-23

&nbsp;

### Viernes - 2023-02-24

&nbsp;

--------------------------------------------------------------------------------

## Semana 5	📝	📔

### Lunes - 2023-02-27

<a name="examen-1" id="examen-1"></a>

!!! danger
    Primer **examen parcial**: Introducción y capa física

    - <https://moodle.fciencias.unam.mx/cursos/mod/quiz/view.php?id=68560>

### Martes - 2023-02-28

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="entrega-practica-2" id="entrega-practica-2"></a>

!!! warning
    Entrega `practica-2` `²`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-2>

    `²`: A partir de esta práctica la entrega es **en equipo**

<a name="practica-3" id="practica-3"></a>

!!! note
    **Práctica 3**: Configuración de VLAN en switches administrables

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-3>

### Miércoles - 2023-03-01

| Elemento           | URL
|:------------------:|:--------------------------------------------------------|
| Capa física - Dispositivos de red | <https://youtu.be/i-Po5Ywhkfg>
| Capa de enlace - Dispositivos de red | <https://youtu.be/-2c2YG8wSrM>
| Capa de red - Dispositivos de red | <https://youtu.be/kcuWhn0J4xk>

### Jueves - 2023-03-02

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

### Viernes - 2023-03-03

&nbsp;

--------------------------------------------------------------------------------

## Semana 6	📔

<a name="capa-3" id="capa-3"></a>

- **Capa 3** - Red

### Lunes - 2023-03-06

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-4" id="practica-4"></a>

!!! note
    **Práctica 4**: Configuración de routers

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-4>

### Martes - 2023-03-07

<a name="entrega-practica-3" id="entrega-practica-3"></a>

!!! warning
    Entrega `practica-3`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-3>

### Miércoles - 2023-03-08

<div class="dia-8m">8M</div>

### Jueves - 2023-03-09

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

### Viernes - 2023-03-10

&nbsp;

--------------------------------------------------------------------------------

## Semana 7	📔

### Lunes - 2023-03-13

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-5" id="practica-5"></a>

!!! note
    **Práctica 5**: Configuración de algoritmos de ruteo

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-5>

### Martes - 2023-03-14

<a name="entrega-practica-4" id="entrega-practica-4"></a>

!!! warning
    Entrega `practica-4`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-4>

### Miércoles - 2023-03-15

&nbsp;

### Jueves - 2023-03-16

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

### Viernes - 2023-03-17

&nbsp;

--------------------------------------------------------------------------------

## Semana 8	🔵

### Lunes - 2023-03-20

<div class="dia-feriado">Día feriado</div>

### Martes - 2023-03-21

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="reasignacion-de-equipos" id="reasignacion-de-equipos"></a>

Reasignación de equipos:

| #	| Estado	| Integrantes	| Equipo
|:-----:|:-------------:|:-------------:|:------------------------------:|
| 1	| 🟠 ¹		| 5		| **Equipo-DAE-RMDE-ZCDA-NVR-VMJM**
| 2	| 🔵 &nbsp;	| 4		| Equipo-AGR-HADB-HJFD-TEJC
| 3	| 🟣 ²		| 5		| **Equipo-ANLE-GVII-GGJ-LPEE-SCMA**
| 4	| 🔵 &nbsp;	| 4		| Equipo-CCU-MGR-PCES-RSGA
| 5	| 🔵 &nbsp;	| 4		| Equipo-GCJ-MAVF-RGA-YZJP
| 6	| 🟣 ³		| 5		| **Equipo-NNA-PSJ-RFOD-RRI-AVJA**
| 7	| 🔵 &nbsp;	| 5		| Equipo-AAR-ATDI-BME-DAAV-LMAM
| 8	| 🔵 &nbsp;	| 5		| Equipo-ACAA-MPKA-PMJM-RRSA-VGI
| 9	| 🔵 &nbsp;	| 5		| Equipo-AGKI-BHD-DTGM-NJMR-SVEL
|10	| 🔵 &nbsp;	| 5		| Equipo-BRRA-GTHL-MMM-OGA-VGM

Notas:

- 🔵  	Sin cambios
- 🟠 ¹	Se juntan los dos equipos de 3 personas
- 🟣 ²	Se integra **SCMA** a un equipo de 4 personas
- 🟣 ³	Se integra **AVJA** a un equipo de 4 personas

<a name="tarea-4" id="tarea-4"></a>

!!! note
    <s>**Tarea 4**: Captura de tráfico de red</s> `¹ ²`

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-4>

    `¹`: <s>Esta tarea debe ser entregada **en equipo**, todas las demás son **individuales**</s>

    `²`: Se <u>**CANCELA**</u> la entrega de la `tarea-4` por las actividades del [paro activo 🟥⬛](#paro-1)

    <s>Realizar <u>esta y las siguientes actividades</u> con la [_nueva_ asignación de equipos](#reasignacion-de-equipos)</s>

### Miércoles - 2023-03-22

&nbsp;

### Jueves - 2023-03-23

&nbsp;

<a name="entrega-practica-5" id="entrega-practica-5"></a>

!!! warning
    Entrega `practica-5`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-5>

    Realizar esta actividad la asignación de equipos _anterior_

### Viernes - 2023-03-24

&nbsp;

--------------------------------------------------------------------------------

## Semana 9	📝	🟥⬛

<a name="capa-4" id="capa-4"></a>

- **Capa 4** - Transporte

### Lunes - 2023-03-27

<a name="examen-2" id="examen-2"></a>

!!! danger
    Segundo **examen parcial**: Capa de enlace y capa de red

    - <https://moodle.fciencias.unam.mx/cursos/mod/quiz/view.php?id=69224>

### Martes - 2023-03-28

<a name="entrega-tarea-4" id="entrega-tarea-4"></a>

!!! warning
    - Se <u>**CANCELA**</u> la entrega de la tarea-4 por las actividades del [paro activo 🟥⬛](#paro-1)

<!--
    Entrega `tarea-4`

    - <https://redes-ciencias-unam.gitlab.io/tareas/tarea-4>

    Realizar <u>esta y las siguientes actividades</u> con la [_nueva_ asignación de equipos](#reasignacion-de-equipos)
-->

<a name="paro-1" id="paro-1"></a>
<div class="paro">
  <p>
    Paro activo
    <br/>
    <a href="https://www.fciencias.unam.mx/institucion/acerca-de/comunicados/comunicadosfc/2023-0016">
      Comunicado Dirección <b>FC/SCDC/2023/016</b>
      <br/>
      Sobre los resultados de la consulta realizada por la Asamblea el día 27 de marzo de 2023
    </a>
  </p>
</div>

### Miércoles - 2023-03-29

<div class="paro">
  Paro activo
  <br/>
  <a href="https://www.fciencias.unam.mx/institucion/acerca-de/comunicados/comunicadosct/2023-0003">
    Comunicado Consejo Técnico <b>FC/CT/2023/003</b>
    <br/>
    Sobre la terminación del paro estudiantil
  </a>
</div>

### Jueves - 2023-03-30

<a name="paro-2" id="paro-2"></a>
<div class="paro">
  Paro activo
  <br/>
  <a href="https://www.fciencias.unam.mx/institucion/acerca-de/comunicados/comunicadosct/2023-0004">
    Comunicado Consejo Técnico <b>FC/CT/2023/004</b>
    <br/>
    Sobre las actividades académicas
  </a>
</div>

### Viernes - 2023-03-31

<div class="paro">Paro activo</a></div>

--------------------------------------------------------------------------------

## Semana 10	🏝️

<a id="vacaciones" name="vacaciones"></a>
<a name="vacaciones-semana-santa" id="vacaciones-semana-santa"></a>

- 🏖️	Vacaciones de semana santa	🏝️

<div class="vacaciones">🏖️	🏝️</div>

<!--
### Lunes - 2023-04-10

&nbsp;

### Martes - 2023-04-11

&nbsp;

### Miércoles - 2023-04-12

&nbsp;

### Jueves - 2023-04-13

&nbsp;

### Viernes - 2023-04-14

&nbsp;
-->

--------------------------------------------------------------------------------

## Semana 11	📔

- **Capa 4** - Transporte

### Lunes - 2023-04-10

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-6" id="practica-6"></a>

!!! note
    **Práctica 6**: Configuración de un firewall de red con pfSense	`¹`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-6>

    Realizar <u>esta y las siguientes actividades</u> con la [_nueva_ asignación de equipos](#reasignacion-de-equipos)

    - `¹`: Se pospuso esta práctica **una semana** por las actividades del [paro activo 🟥⬛](#paro-1)

### Martes - 2023-04-11

&nbsp;

### Miércoles - 2023-04-12

&nbsp;

### Jueves - 2023-04-13

&nbsp;

### Viernes - 2023-04-14

&nbsp;

--------------------------------------------------------------------------------

## Semana 12	📔

### Lunes - 2023-04-17

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-7" id="practica-7"></a>

!!! note
    **Práctica 7**: Implementación de un servidor de archivos con **NFS** y **Samba**	`¹`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-7>

    Realizar <u>esta y las siguientes actividades</u> con la [_nueva_ asignación de equipos](#reasignacion-de-equipos)

    - `¹`: Se pospuso esta práctica **una semana** por las actividades del [paro activo 🟥⬛](#paro-1)

### Martes - 2023-04-18

&nbsp;

### Miércoles - 2023-04-19

<a name="entrega-practica-6" id="entrega-practica-6"></a>

!!! warning
    Entrega `practica-6`	`¹`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-6>

    Se dio una extensión de dos días para entregar la práctica.

    - No habrá más extensiones para esta práctica porque ya se dió tiempo ADICIONAL para la elaboración de la actividad
    - En otras palabras, esta fecha de entrega (del miércoles) «NO SE MOVERÁ» si la Facultad de Ciencias entra en paro de nuevo 🟥⬛

### Jueves - 2023-04-20

<a name="paro-3" id="paro-3"></a>
<div class="paro">
  Paro activo
  <br/>
  <a href="https://www.fciencias.unam.mx/institucion/acerca-de/comunicados/comunicadosfc/2023-0019">
    Comunicado Dirección <b>FC/SCDC/2023/019</b>
    <br/>
    Sobre los próximos paros activos
  </a>
</div>

### Viernes - 2023-04-21

&nbsp;

--------------------------------------------------------------------------------

## Semana 13	📔

### Lunes - 2023-04-24

<a name="paro-3" id="paro-3"></a>
<div class="paro">
  Paro activo
  <br/>
  <a href="https://www.fciencias.unam.mx/institucion/acerca-de/comunicados/comunicadosfc/2023-0019">
    Comunicado Dirección <b>FC/SCDC/2023/019</b>
    <br/>
    Sobre los próximos paros activos
  </a>
</div>

### Martes - 2023-04-25

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-8" id="practica-8"></a>

!!! note
    **Práctica 8**: Registro de dominio DNS y creación de recursos en la nube

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-8>

    - `¹`: Se pospuso esta práctica **una semana** por las actividades del [paro activo 🟥⬛](#paro-1)
    - `²`: Se volvió a reagendar esta práctica por las actividades del [paro activo 🟥⬛](#paro-3)

### Miércoles - 2023-04-26

<a name="entrega-practica-7" id="entrega-practica-7"></a>

!!! warning
    Entrega `practica-7`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-7>

    `¹`: Se pospuso esta entrega por las actividades del [paro activo 🟥⬛](#paro-3)

### Jueves - 2023-04-27

&nbsp;

### Viernes - 2023-04-28

<a name="paro-3" id="paro-3"></a>
<div class="paro">
  Paro activo
  <br/>
  <a href="https://www.fciencias.unam.mx/institucion/acerca-de/comunicados/comunicadosfc/2023-0019">
    Comunicado Dirección <b>FC/SCDC/2023/019</b>
    <br/>
    Sobre los próximos paros activos
  </a>
</div>

--------------------------------------------------------------------------------

## Semana 14	📔

### Lunes - 2023-05-01

<div class="dia-feriado">Día feriado</div>

### Martes - 2023-05-02

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="practica-9" id="practica-9"></a>

!!! note
    **Práctica 9**: Implementación de sitios web sobre HTTPS

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-9>

    - `¹`: Se pospuso esta práctica **una semana** por las actividades del [paro activo 🟥⬛](#paro-1)
    - `²`: Se volvió a reagendar esta práctica por las actividades del [paro activo 🟥⬛](#paro-3)

### Miércoles - 2023-05-03

&nbsp;

### Jueves - 2023-05-04

<a name="entrega-practica-8" id="entrega-practica-8"></a>

!!! warning
    Entrega `practica-8`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-8>

    `¹`: Se pospuso esta entrega por las actividades del [paro activo 🟥⬛](#paro-3)

### Viernes - 2023-05-05

<!-- Asamblea -->
&nbsp;

--------------------------------------------------------------------------------

## Semana 15	📝

### Lunes - 2023-05-08

&nbsp;

### Martes - 2023-05-09

&nbsp;

<!--
???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="proyecto-1" id="proyecto-1"></a>

!!! note
    **Proyecto 1**: Implementación de una VPN

    - <https://redes-ciencias-unam.gitlab.io/proyectos/proyecto-vpn>
-->

### Miércoles - 2023-05-10

<div class="dia-feriado">Día feriado</div>

### Jueves - 2023-05-11

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="examen-3" id="examen-3"></a>

!!! note
    **Examen 3**: Implementación de un sitio web de producción

    - <https://redes-ciencias-unam.gitlab.io/examenes/examen-web>

<a name="entrega-practica-9" id="entrega-practica-9"></a>

!!! warning
    Entrega `practica-9`

    - <https://redes-ciencias-unam.gitlab.io/laboratorio/practica-9>

    `¹`: Se pospuso esta entrega por las actividades del [paro activo 🟥⬛](#paro-3)

### Viernes - 2023-05-12

&nbsp;

--------------------------------------------------------------------------------

## Semana 16

### Lunes - 2023-05-15

<div class="dia-feriado">Día feriado</div>

### Martes - 2023-05-16

&nbsp;

### Miércoles - 2023-05-17

&nbsp;

### Jueves - 2023-05-18

&nbsp;

### Viernes - 2023-05-19

&nbsp;

--------------------------------------------------------------------------------

## Semana 17	📝

### Lunes - 2023-05-22

???+ zoom
    - <https://calendar.google.com/a/ciencias.unam.mx>

<a name="examen-4" id="examen-4"></a>

!!! note
    **Examen 4**: Implementación de sitios web en Kubernetes

    - <https://redes-ciencias-unam.gitlab.io/examenes/examen-kubernetes>

### Martes - 2023-05-23

<a name="entrega-examen-3" id="entrega-examen-3"></a>

!!! warning
    Entrega `examen-3`

    - <https://redes-ciencias-unam.gitlab.io/examenes/examen-web>

### Miércoles - 2023-05-24

&nbsp;

### Jueves - 2023-05-25

&nbsp;

### Viernes - 2023-05-26

&nbsp;

--------------------------------------------------------------------------------

## Semana 18

- Inicia periodo de evaluación
- Primera vuelta de finales

### Lunes - 2023-05-29

<!-- Paro propuesto -->
&nbsp;

### Martes - 2023-05-30

&nbsp;

### Miércoles - 2023-05-31

&nbsp;

### Jueves - 2023-06-01

&nbsp;

### Viernes - 2023-06-02

<a name="entrega-examen-4" id="entrega-examen-4"></a>

!!! warning
    Entrega `examen-4`

    - <https://redes-ciencias-unam.gitlab.io/examenes/examen-kubernetes>

--------------------------------------------------------------------------------

## Semana 19	🔖

- Segunda vuelta de finales

### Lunes - 2023-06-05

&nbsp;

### Martes - 2023-06-06

&nbsp;

### Miércoles - 2023-06-07

&nbsp;

### Jueves - 2023-06-08

&nbsp;

### Viernes - 2023-06-09

&nbsp;

--------------------------------------------------------------------------------

<!--
## Semana 20

- Finaliza periodo de evaluación

### Lunes - 2023-06-12

&nbsp;

### Martes - 2023-06-13

&nbsp;

### Miércoles - 2023-06-14

&nbsp;

### Jueves - 2023-06-15

&nbsp;

### Viernes - 2023-06-16

&nbsp;
-->

--------------------------------------------------------------------------------

|
|:--:|
| [![][pipeline-status]][pipelines]

--------------------------------------------------------------------------------

[pipelines]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/notas/-/pipelines
[pipeline-status]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/notas/badges/main/pipeline.svg

[zoom]: https://zoom.us/

[tarea-1]: https://redes-ciencias-unam.gitlab.io/tareas/tarea-1
[tarea-2]: https://redes-ciencias-unam.gitlab.io/tareas/tarea-2
[tarea-3]: https://redes-ciencias-unam.gitlab.io/tareas/tarea-3
[tarea-4]: https://redes-ciencias-unam.gitlab.io/tareas/tarea-4

[practica-1]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-1
[practica-2]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-2
[practica-3]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-3
[practica-4]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-4
[practica-5]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-5
[practica-6]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-6
[practica-7]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-7
[practica-8]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-8
[practica-9]: https://redes-ciencias-unam.gitlab.io/laboratorio/practica-9

[proyecto-1]: https://redes-ciencias-unam.gitlab.io/proyectos/proyecto-1

[examen-1]: https://redes-ciencias-unam.gitlab.io/examenes/examen-1
[examen-2]: https://redes-ciencias-unam.gitlab.io/examenes/examen-2
[examen-3]: https://redes-ciencias-unam.gitlab.io/examenes/examen-3
[examen-4]: https://redes-ciencias-unam.gitlab.io/examenes/examen-4
