#!/bin/bash
#	= ^ . ^ =

set -o pipefail
set -xu

printenv PATH

which   python python3 pip pip3 | sed -e 's|\s+|\n|g'
whereis python python3 pip pip3

ls -ld /usr/local/*/python*

PYTHON_DIRS=$(find /usr/local -type d -iname 'python*')
PYTHON_EXECUTABLES=$(find /usr/local/bin /usr/local/sbin ! -type d \( -iname 'python*' -o -iname 'pip*' \) -executable)

for DIR in ${PYTHON_DIRS}
do
  # Quitar 'echo' si el comando muestra una salida válida
  echo mv -v "${DIR}" "_${DIR}_old"
done

for FILE in ${PYTHON_EXECUTABLES}
do
  # Quitar 'echo' si el comando muestra una salida válida
  echo mv -v "${FILE}" "_${FILE}_old"
done
