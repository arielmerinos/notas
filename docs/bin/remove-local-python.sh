#!/bin/bash
#	= ^ . ^ =

set -uo pipefail

PREFIX="/usr/local"

PYTHON_DIRS="
/usr/local/lib/python2.7
/usr/local/include/python2.7

/usr/local/lib/python3.8
/usr/local/include/python3.8
"

PYTHON_EXECUTABLES="
/usr/local/bin/2to3
/usr/local/bin/idle
/usr/local/bin/pip
/usr/local/bin/pydoc
/usr/local/bin/python
/usr/local/bin/python-config
/usr/local/bin/python2
/usr/local/bin/python2-config
/usr/local/bin/python2.7
/usr/local/bin/python2.7-config

/usr/local/bin/2to3-3.8
/usr/local/bin/idle3
/usr/local/bin/idle3.8
/usr/local/bin/pip3
/usr/local/bin/pip3.8
/usr/local/bin/pydoc3
/usr/local/bin/pydoc3.8
/usr/local/bin/python3
/usr/local/bin/python3-config
/usr/local/bin/python3.8
/usr/local/bin/python3.8-config
/usr/local/bin/smtpd.py

/usr/local/lib/libpython2.7.a
/usr/local/lib/libpython3.8.a
"

PYTHON_FILES="
/usr/local/lib/pkgconfig/python*
/usr/local/man/man1/python*
"

for ITEM in ${PYTHON_DIRS} ${PYTHON_EXECUTABLES} ${PYTHON_FILES}
do
  test -e "${ITEM}" || continue
  BASE=$(dirname ${ITEM})
  ITEM=$(basename ${ITEM})
  # Quitar 'echo' si el comando muestra una salida válida
  echo mv -v "${BASE}/${ITEM}" "${BASE}/_${ITEM}_old"
done

set -x

find ${PREFIX} -name '_*_old' -ls

printenv PATH

which   python python3 pip pip3 | sed -e 's|\s\+|\n  |g'
whereis python python3 pip pip3 | sed -e 's|:\s\+|:\n|g' -e 's|\s\+|\n  |g'

ls -l /usr/local
ls -ld /usr/local/*/python*
