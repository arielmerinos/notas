#!/bin/bash
#	= ^ . ^ =
APT_CONF_LOCAL=/etc/apt/apt.conf.d/99-local
export DEBIAN_FRONTEND="noninteractive"
export PAGER="cat"

set -o pipefail
set -evxu

truncate --size=0 "${APT_CONF_LOCAL}"
cat > "${APT_CONF_LOCAL}" << EOF
quiet "2";
APT::Get::Assume-Yes "1";
APT::Install-Recommends "0";
APT::Install-Suggests "0";
APT::Color "0";
Dpkg::Progress "0";
Dpkg::Progress-Fancy "0";
EOF

apt update
apt install make

pip config --global set global.progress_bar off
pip install -r requirements.txt
